const express = require("express");
const axios = require("axios");
const app = express();

app.get("/api/search/:keyword", async (req, res) => {
  const series = await axios.get(
    `https://www.alphavantage.co/query?function=SYMBOL_SEARCH&keywords=${req.params.keyword}&apikey=WRLP9HSCN4U2L0UP`
  );
  res.send(series.data);
});

app.listen(5000, () => console.log("Example app listening on port 5000!"));
