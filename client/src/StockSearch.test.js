import React from 'react';
import { render, act } from '@testing-library/react';
import ReactTestUtils from 'react-dom/test-utils';
import StockSearch from './StockSearch';

test('renders search input', () => {
  const { container } = render(<StockSearch />);
  const searchInput = container.querySelector('input[data-testid="search-stock"]');
  expect(searchInput).toBeInTheDocument();
});

test('call search api on text input', () => {
  const mockedSearchResult = {
    "bestMatches": [
        {
            "1. symbol": "BA",
            "2. name": "The Boeing Company",
            "3. type": "Equity",
            "4. region": "United States",
            "5. marketOpen": "09:30",
            "6. marketClose": "16:00",
            "7. timezone": "UTC-05",
            "8. currency": "USD",
            "9. matchScore": "1.0000"
        }
      ]
    }

  jest.spyOn(global, "fetch").mockImplementation(() =>
    Promise.resolve({
      json: () => Promise.resolve(mockedSearchResult)
    })
  );

  const { container, debug } = render(<StockSearch />);
  act(() =>{
    const searchInput = container.querySelector('input[data-testid="search-stock"]');
    searchInput.value = 'ba';
    ReactTestUtils.Simulate.change(searchInput)

    debug() //TODO: does not wait for popup suggestions to show

    const suggestionsList = container.querySelector('.react-autosuggest__suggestions-list');

    expect(suggestionsList.textContent).toBe('The Boeing Company');

    global.fetch.mockRestore();
  })

})
