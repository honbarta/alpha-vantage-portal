import React, { useState, useEffect } from "react"
import Autosuggest from "react-autosuggest"

function StockSearch() {
  const [stockName, setStockName] = useState("");
  const [isLoading, setIsLoading] = useState(true);
  const [suggestions, setSuggestions] = useState([]);
  const [selectedStock, setSelectedStock] = useState({});

  const loadSuggestions = value => {
    fetch(`/api/search/${value}`, {
      method: "GET"
    })
      .then(res => res.json())
      .then(response => {
        setSuggestions(response.bestMatches);
      })
      .catch(error => console.log(error));
  };

  const onSuggestionsFetchRequested = ({ value }) => {
    loadSuggestions(value);
  };

  const onSuggestionsClearRequested = () => {
    setSuggestions([]);
  };

  const getSuggestionValue = (suggestion) => {
    return suggestion["2. name"];
  }

  const renderSuggestion = (suggestion) => {
    return (
      <span>{suggestion["2. name"]}</span>
    );
  }

  const onChange = (event, { newValue }) => {
    setStockName(newValue)
  };

  const inputProps = {
    placeholder: "Select stock",
    value: stockName,
    onChange,
    ["data-testid"]: "search-stock"
  };

  const loadStockDetail = (event, { suggestion, suggestionValue, suggestionIndex, sectionIndex, method }) => {
    setSelectedStock(suggestion)
  }

  return (
    <div>
      <Autosuggest
        suggestions={suggestions}
        onSuggestionsFetchRequested={onSuggestionsFetchRequested}
        onSuggestionsClearRequested={onSuggestionsClearRequested}
        getSuggestionValue={getSuggestionValue}
        renderSuggestion={renderSuggestion}
        inputProps={inputProps}
        onSuggestionSelected={loadStockDetail}
      />

      {JSON.stringify(selectedStock)}

      {/* {isLoading && <p>Loading</p>}

      {!isLoading &&
        stock.map((s, index) => <div key={index}>{s["2. name"]}</div>)} */}
    </div>
  );
}

export default StockSearch;
