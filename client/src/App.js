import React from 'react';
import './App.css';
import StockSearch from "./StockSearch";

function App() {
  return (
    <div className="App">
      <header className="App-header">
      </header>
      <StockSearch />
    </div>
  );
}

export default App;
