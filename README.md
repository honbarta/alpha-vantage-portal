# Alpha Vantage Portal

## What it contains

### Server

Node.js Express server with one endpoint exposing alpha vantage search portal.

*What is not there*

- validation
    - I would add some simple server validation. Probably not for search endpoint but maybe for the comparison
- tests
    - Ideally I would add there integration tests, testing that vantage API has not changed and works as expected
    - Currently there is no business logic so no unit tests are needed. But when there is some validation, or comparison of two prices, I would add unit tests for those
- linting - I would setup eslint with some recommended settings like airbnb

### Client

React Create App with one element - search input - which call server on user input. There is one unit test for component render and unfinished integration test which should mock HTTP request and test integration of the component with API but there is a problem there which I haven't had time to fix.

*What's not there*
- most of the requested logic
- styling
    - I would use maybe ant.d library for built-in styles and basic React components
    -
- linting
- working integration test
- caching of requests in the browser
- responsiveness
- Gitlab CI - I would leverage Gitlab CI to run test on each commit

## How to run it

```
cd server
npm start
```

```
cd client
npm start
```